FROM node:13-alpine
WORKDIR /usr/src/app
RUN npm install -g @angular/cli@8.3.25
RUN npm install -g http-server
COPY ./dist .
EXPOSE 80
ENTRYPOINT ["http-server", "-p", "80"]
