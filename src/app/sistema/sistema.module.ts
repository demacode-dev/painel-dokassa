import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ToastModule } from 'primeng/toast';
import { TableModule } from 'primeng/table';
import { MessageService } from 'primeng/api';
import { NgxMaskModule } from 'ngx-mask'

import { SistemaComponent } from './sistema.component';
import { CoreModule } from '../core/core.module';
import { ImagePreloadDirective } from '../shared/directives/ImagePreload.directive';
import { SideBarModule } from '../core/side-bar/side-bar.module';
import { HomeComponent } from './home/home.component';
import { HeaderModule } from '../core/header/header.module';
import { DropdownModule } from 'primeng/dropdown';


@NgModule({
    declarations: [
        SistemaComponent,
        ImagePreloadDirective,
        HomeComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        RouterModule,
        FormsModule,
        MessagesModule,
        MessageModule,
        ToastModule,
        SideBarModule,
        HeaderModule,
        TableModule,
        DropdownModule,
        NgxMaskModule.forRoot()
    ],
    providers: [
        MessageService
    ]
})
export class SistemaModule { }
