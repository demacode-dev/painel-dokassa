import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Push } from 'src/app/shared/interfaces/Push';

@Injectable()
export class NotificacaoService {

    constructor(private http: HttpClient) { }

    enviarNotificacao(push:Push): Observable<Push> {
        return this.http.post(environment.URL_BASE + "WS/NotificacaoApp/Enviar", push);
    }

}