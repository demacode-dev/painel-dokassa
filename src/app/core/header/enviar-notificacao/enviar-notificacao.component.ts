import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';

import { Push } from 'src/app/shared/interfaces/Push';
import { NotificacaoService } from 'src/app/sistema/services/notificacao.service';
import { MessageService } from 'primeng/api';

@Component({
    selector: 'dc-enviar-notificacao',
    templateUrl: './enviar-notificacao.component.html',
    styleUrls: ['./enviar-notificacao.component.styl'],
    providers: [
        NotificacaoService
    ]
})
export class EnviarNotificacaoComponent implements OnInit, OnDestroy {

    push: Push;

    @ViewChild('popupEnviarNotificacao', { static: true })
    popupEnviarNotificacao: ElementRef<HTMLDivElement>;

    constructor(
        private messageService: MessageService,
        private notificacao: NotificacaoService
    ) { }

    ngOnInit() {
        this.push = {};
    }

    ngOnDestroy() {
    }

    controlarPopupEnviarNotificacao() {
        const popup: HTMLDivElement = this.popupEnviarNotificacao.nativeElement;

        if (popup.style.display === 'none') {
            popup.style.display = 'flex';
            this.push = {};
        } else {
            popup.style.display = 'none';
        }
    }

    enviarMensagem() {
        if(this.push.mensagem && this.push.mensagem.length != 0){
            this.notificacao.enviarNotificacao(this.push).subscribe()
            this.controlarPopupEnviarNotificacao();
            this.messageService.add({
                key: 'toast', 
                severity:'success', 
                summary: 'Sucesso', 
                detail: 'Mensagem enviada com sucesso'
            });
        }else{
            this.messageService.add({
                key: 'toast', 
                severity:'error', 
                summary: 'Erro', 
                detail: 'Você precisa preencher o campo mensagem'
            });
        }
    }
}
