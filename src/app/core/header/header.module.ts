import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { EnviarNotificacaoComponent } from './enviar-notificacao/enviar-notificacao.component';
import { HeaderComponent } from './header.component';


@NgModule({
    declarations: [
        EnviarNotificacaoComponent,
        HeaderComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        FormsModule
    ],
    exports: [
        HeaderComponent
    ]
})
export class HeaderModule { }
