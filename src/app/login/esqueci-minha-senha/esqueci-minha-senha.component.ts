import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { LoginService } from '../login.service';
import { Router } from '@angular/router';

@Component({
	templateUrl: './esqueci-minha-senha.component.html',
	styleUrls: ['./esqueci-minha-senha.component.styl']
})
export class EsqueciMinhaSenhaComponent implements OnInit {

	esqueciForm: FormGroup;
	@ViewChild('btnEnviar', {static: true}) btnEnviar: ElementRef<HTMLButtonElement>;
	@ViewChild('senhaEnviada', {static: true}) pSenhaEnviada: ElementRef<HTMLParagraphElement>;

	constructor(
		private formBuilder: FormBuilder,
		private title: Title,
		private messageService: MessageService,
		private loginService: LoginService,
		private router: Router
	) {}

	ngOnInit() {
		this.title.setTitle('Esqueci minha senha | Dokasse');

		this.esqueciForm = this.formBuilder.group({
			email: ['', 
				[Validators.required, Validators.email]
			],
		});

	}

	esqueciSenha() {
		this.loginService.recuperarSenha(this.esqueciForm.get('email').value).subscribe(
			response => {
				this.messageService.add({
					key: 'toast', 
					severity:'success', 
					summary: 'Sucesso', 
					detail: 'Uma senha foi encaminhada ao seu e-mail.'
				});
				this.router.navigate(['/login']);
			},
			error => {
				if(error.status == 200){
					this.messageService.add({
						key: 'toast', 
						severity:'success', 
						summary: 'Sucesso', 
						detail: 'Uma senha foi encaminhada ao seu e-mail.'
					});
					this.router.navigate(['/login']);
				}else{
					this.messageService.add({
						key: 'toast',
						severity:'error', 
						summary: 'Erro', 
						detail: 'E-mail incorreto.'
					});
				}
			}
		);
	}
}
