import { Direction } from '../enums/Direction';

export interface Sort {
    direction?: Direction;
    property?: string;
    ignoreCase?: boolean;
    nullHandling?: string;
    ascending?: boolean;
    descending?: boolean;
}
