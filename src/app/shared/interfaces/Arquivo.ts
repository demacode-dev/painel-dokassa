export interface Arquivo {
    bytes?: number;
    tipoArquivo?: number;
    nome?: string;
    tamanho?: number;
    chaveTamanho?: string;
    contentType?: string;
    url?: string;
}
