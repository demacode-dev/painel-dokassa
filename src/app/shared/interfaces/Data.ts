import { BaseEntity } from './BaseEntity';
import { SimNao } from '../enums/SimNao';

export interface Data {
    id?: string;
    tipo?: string;
    dataInclusao?: string;
    idOrigem?: string;
    idDestino?: string;
    json?: string;
}
