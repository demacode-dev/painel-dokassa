import { Arquivo } from './Arquivo';

export interface Usuario {
    id?: number;
    nome?: string;
    email?: string;
    foto?: Arquivo;
}