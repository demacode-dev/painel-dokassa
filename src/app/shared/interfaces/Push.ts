import { BaseEntity } from './BaseEntity';

export interface Push extends BaseEntity {
    titulo?: string;
    mensagem?: string;
}
