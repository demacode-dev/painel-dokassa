import { BaseEntity } from './BaseEntity';
import { SimNao } from '../enums/SimNao';
import { Data } from './Data';

export interface Log extends BaseEntity {
    status?: string;
    statusMsg?: string;
    dataUltimoEnvio?: Date;
    dataProximoEnvio?: Date;
    seNotificado?: string;
    usuarioMensagem?: string;
    data?: Data;
}
