import { Sort } from './Sort';

export interface Paginacao<T> {
    content?: T[];
    last?: boolean;
    totalElements?: number;
    totalPages?: number;
    sort?: Sort[];
    first?: boolean;
    numberOfElements?: number;
    size?: number;
    number?: number;
}
