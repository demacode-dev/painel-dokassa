export interface Endereco {
    cidade?: string;
    estado?: string;
    uf?: string;
    rua?: string;
    numero?: string;
    bairro?: string;
}
