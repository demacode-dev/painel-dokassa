export enum Situacao {
    Ativo = "A",
    Excluido = "E",
    Inativo = "I"
}
