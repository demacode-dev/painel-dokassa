import { NgModule, LOCALE_ID } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { registerLocaleData } from '@angular/common';

import { LoginComponent } from './login/login.component';
import { AcessarComponent } from './login/acessar/acessar.component';
import { EsqueciMinhaSenhaComponent } from './login/esqueci-minha-senha/esqueci-minha-senha.component';
import { SistemaComponent } from './sistema/sistema.component';
import { AuthenicationGuard } from './core/guard/authentication.guard';
import { HomeComponent } from './sistema/home/home.component';
import ptBr from '@angular/common/locales/pt';

registerLocaleData(ptBr);

const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent,
        children: [
            {
                path: '',
                component: AcessarComponent
            },
            {
                path: 'esqueci-minha-senha',
                component: EsqueciMinhaSenhaComponent
            }
        ]
    },
    {
        path: '',
        component: SistemaComponent,
        canActivate: [
         	AuthenicationGuard
        ],
        children: [
            {
                path: '',
                component: HomeComponent
            }
        ]
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {
        scrollPositionRestoration: 'top'
    })],
    exports: [RouterModule],
    providers: [{ provide: LOCALE_ID, useValue: 'pt' }],
})
export class AppRoutingModule { }
